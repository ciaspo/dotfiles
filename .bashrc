#   _               _              
#  | |             | |             
#  | |__   __ _ ___| |__  _ __ ___ 
#  | '_ \ / _` / __| '_ \| '__/ __|
#  | |_) | (_| \__ \ | | | | | (__ 
#  |_.__/ \__,_|___/_| |_|_|  \___|
#                                  
#                                  
if [ -f ~/.git-completion.bash ]; then
	source ~/.git-completion.bash
fi

# set PATH so it includes user's private bin if it exists

if [ -d "$HOME/bin" ] ; then
	PATH="$HOME/bin:$PATH"
fi

# includes /opt in the PATH
PATH="/opt:$PATH"

# extend the capacity of ~/.bash_history file

export HISTSIZE=10000
export HISTFILESIZE=1000000			# 500 is the default

# adds date to commands in ~/.bash_history

export HISTTIMEFORMAT='%b %d %I:%M %p '		# using strftime format

# sets commands to ignore in ~/.bash_history

export HISTCONTROL=ignoreboth			# ignoredups:ignorespace
export HISTIGNORE="history:pwd:exit:df:ls:ls -al:ll"

# increase the speed of repetition when pressing a key
xset r rate 300 50

# set the prompt to formats that I tested in the past
# export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
# export PS1="\[\e[31m\]\\$\[\e[m\] \[\e[31m\]\w >\[\e[m\] "

if [ "$color_prompt" = yes ]; then
# set the prompt to Luke Smith's style modified with Git branch.
  
  parse_git_branch() {
       git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
  }
 PS1="\${debian_chroot:+($debian_chroot)}\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h\[$(tput setaf 5)\] \W\[$(tput setaf 2)\]\$(parse_git_branch)\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ -> \[$(tput sgr0)\]"
  else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# set the prompt to Luke Smith's style modified with Git branch.

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h\[$(tput setaf 5)\] \W\[$(tput setaf 2)\]\$(parse_git_branch)\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ -> \[$(tput sgr0)\]"

# colorise the matches of grep

export GREP_OPTIONS="--color=auto"

# set extended grep by default

export GREP_OPTIONS="-E"

# set bash and ls commands to show colours

export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

# handy command aliases to use as shortcuts

alias ls='ls -GFh'
alias ll='ls -alh'
alias linode='ssh alberto@212.111.40.251'
alias h='history'
alias t='task'
alias nt="task add $1"
alias tl="task list"
alias reload="source .bashrc"
alias note="cd notes"
alias c='clear'
alias taskell="~/Dropbox/taskell/ && taskell"

alias vim="nvim"

alias news="newsboat"
alias mail="mutt"
alias web="lynx"
alias cal="calcurse"

# automatic 'cd' when typing a directory
shopt -s autocd

# shortcut functions for taskwarrior

function TaskProject() {
    echo "Enter a task number \n followed by a project name"
	task $1 modify project:$2
}
alias tp=TaskProject

function TaskTag() {
	task $1 modify +$2 +$3 +$4
}
alias tt=TaskTag

# Create an alias to use git on the dotfiles directory
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

# start neofetch to show system info at bash launch

/usr//bin/neofetch

TerminalMargin=4
