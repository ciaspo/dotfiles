"                    
"        (_)                   
"  __   ___ _ __ ___  _ __ ___ 
"  \ \ / / | '_ ` _ \| '__/ __|
"   \ V /| | | | | | | | | (__ 
"    \_/ |_|_| |_| |_|_|  \___|
"
"=============================="
"	     Some Basics           "
"=============================="
"
    " ~~~~~ Basic highlight
        syntax on

    " ~~~~~ Set the vim theme
    "   colorscheme gruvbox
        set background=dark
    " ~~~~~ Set the background transparent
        hi Normal guibg=NONE ctermbg=NONE
"       highlight Normal ctermbg=white
        if executable('rg')
            let g:rg_derive_root='true'
        endif

    " ~~~~~ Wrapping keeping words whole
        set wrap
        set linebreak

    " ~~~~~ No alert sound on errors
        set noerrorbells
    " ~~~~~ Tabs to 4 spaces
        set tabstop=4 softtabstop=4
    " ~~~~~ Indents to 4 spaces
        set shiftwidth=4
        set expandtab
    " ~~~~~ Line numbering in the relative way
        set nu rnu
"       set nowrap
    " ~~~~~ Case insensitive unless a capital is typed while searching
        set smartcase
        set smartindent
        set nocompatible
        set exrc
        set noswapfile
        set nobackup
        set undodir=~/.vim/undodir
        set undofile
        set encoding=utf-8
        set clipboard=unnamedplus
        set incsearch
    " ~~~~~ Treat all numerals as decimals regardless of whether they are
    " ~~~~~ padded with zeroes
        set nrformats=
    " ~~~~~ Hide -- INSERT -- from the status line
        set noshowmode
    " ~~~~~ Set a vertical guide at the 77th column
        " set colorcolumn=77
        " highlight ColorColumn ctermbg=darkgray guibg=blue
        " filetype plugin indent on
    " ~~~~~ Sets the status line always on
        set laststatus=2
    " ~~~~~ Activate spell check
        "set spell
    " ~~~~~ Set spell languages
        set spelllang=en_gb,it
    " ~~~~~ Splits usually open above and on the left, 
    " ~~~~~ this command sets them more intuitively
        set splitbelow splitright

"=============================="
"	        Plugins            "
"=============================="
 
    " ~~~~~ For instructions about Plug see: https://github.com/junegunn/vim-plug/wiki/tutorial

    " ~~~~~ Plugins will be downloaded under the specified directory.
        call plug#begin('~/.vim/plugged')

        Plug 'jremmen/vim-ripgrep'
        Plug 'morhetz/gruvbox'
        Plug 'vim-utils/vim-man'
        Plug 'kien/ctrlp.vim'
        Plug 'leafgarland/typescript-vim'
        Plug 'tpope/vim-fugitive'
        Plug 'mbbill/undotree'
        Plug 'tpope/vim-sensible'
        Plug 'tpope/vim-commentary'
        Plug 'junegunn/seoul256.vim'
        Plug 'masukomi/vim-markdown-folding'
        Plug 'tpope/vim-markdown'
        Plug 'itchyny/lightline.vim'		"Lightline statusbar
        Plug 'scrooloose/nerdtree'
        Plug 'francoiscabrol/ranger.vim'
        Plug 'vimwiki/vimwiki'
        Plug 'mattn/calendar-vim'
        Plug 'alvan/vim-closetag'
        Plug 'tpope/vim-surround'
        Plug 'jiangmiao/auto-pairs'
        Plug 'tbabej/taskwiki'
        Plug 'dhruvasagar/vim-table-mode'
        Plug 'reedes/vim-pencil'

    " ~~~~~ List ends here. Plugins become visible to Vim after this call.
        call plug#end()

        let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
    " ~~~~~ Set the leader to space
        let mapleader = " "

"=============================="
"	        Nettree            "
"=============================="

        let g:netrw_browse_split = 2
        let g:netrw_banner = 0
        let g:netrw_winsize = 25

        let g:ctrlp_use_caching = 0

"=============================="
"	        Mapping            "
"=============================="
        nnoremap <leader>q :wq<CR>
        nnoremap <leader>h :wincmd h<CR>
        nnoremap <leader>j :wincmd j<CR>
        nnoremap <leader>k :wincmd k<CR>
        nnoremap <leader>l :wincmd l<CR>
        nnoremap <leader>u :UndotreeShow<CR>
        nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
        nnoremap <Leader>ps :Rg<SPACE>
        nnoremap <silent> <Leader>+ :vertical resize +5<CR>
        nnoremap <silent> <Leader>- :vertical resize -5<CR>
        vnoremap J :m '>+1<CR>gv=gv
        vnoremap K :m '<-2<CR>gv=gv

    " Remaps to move lines up and down
    " ∆=<Alt-j> and ˚=<Alt-k> 
        nnoremap <C-j> :m .+1<CR>==
        nnoremap <C-k> :m .-2<CR>==
        inoremap <C-j> <Esc>:m .+1<CR>==gi
        inoremap <C-k> <Esc>:m .-2<CR>==gi
        vnoremap <C-j> :m '>+1<CR>gv=gv
        vnoremap <C-k> :m '<-2<CR>gv=gv

    " Remaps to move folded lines up and down
    " the Ô=<Shift-Alt-j> and =<Shift-Alt-k> in Mac 
        nnoremap Ô dd f p
        nnoremap  dd kk p

"=================================="
"             VIMWIKI              "
"=================================="
        let wiki_1 = {}
        let wiki_1.path = '~/Dropbox/vimwiki/'
        let wiki_1.syntax = 'markdown'
        let wiki_1.ext = '.md'

        let g:vimwiki_global_ext = 0
        let g:vimwiki_list = [wiki_1]
        let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}

        au BufRead,BufNewFile *.md set filetype=vimwiki

    " Diary Template
        autocmd FileType markdown inoremap <leader>diary #<Space><++><CR><CR><++><CR><CR>##<Space>DevLog<CR><CR><++><CR><CR><Esc>gg
    " This is for a vim wiki note template
        autocmd Filetype markdown inoremap <leader>zettel ---<CR>File: <CR>Date: <CR>Keywords: []<CR>---<CR><CR># Title<CR><CR>-------------------<CR>## Related Notes<CR><CR>-------------------<CR>## Main References<CR><CR>## Citekey<CR><CR>-------------------<CR>## Other References<CR><Esc>gg2ji
    " This inputs a NOW() timestamp
        autocmd Filetype markdown inoremap <leader>ns *<CR><Esc>!!date<CR>A*<Esc>kJxA<CR><CR>
    " This inputs a YYYYmmddHM timestamp
        autocmd Filetype markdown inoremap <leader>zs <C-R>=strftime("%Y%m%d%H%M")<CR>
    " This inputs a YYYY-mm-dd timestamp
        autocmd Filetype markdown inoremap <leader>ds <C-R>=strftime("%Y-%m-%d")<CR>
    " This inputs the file name
        inoremap <Leader>fn <C-R>=expand("%:t:r")<CR>

"=================================="
"             CLOSETAG             "
"=================================="

    " filenames like *.xml, *.html, *.xhtml, ...
    " These are the file extensions where this plugin is enabled.
    "
    let g:closetag_filenames = '*.html,*.xhtml,*.phtml'

    " filenames like *.xml, *.xhtml, ...
    " This will make the list of non-closing tags self-closing in the specified files.
    "
    let g:closetag_xhtml_filenames = '*.xhtml,*.jsx'

    " filetypes like xml, html, xhtml, ...
    " These are the file types where this plugin is enabled.
    "
    let g:closetag_filetypes = 'html,xhtml,phtml'

    " filetypes like xml, xhtml, ...
    " This will make the list of non-closing tags self-closing in the specified files.
    "
    let g:closetag_xhtml_filetypes = 'xhtml,jsx'

    " integer value [0|1]
    " This will make the list of non-closing tags case-sensitive (e.g. `<Link>` will be closed while `<link>` won't.)
    "
    let g:closetag_emptyTags_caseSensitive = 1

    " dict
    " Disables auto-close if not in a "valid" region (based on filetype)
    "
    let g:closetag_regions = {
    \ 'typescript.tsx': 'jsxRegion,tsxRegion',
    \ 'javascript.jsx': 'jsxRegion',
    \ }

    " Shortcut for closing tags, default is '>'
    "
    let g:closetag_shortcut = '>'

    " Add > at current position without closing the current tag, default is ''
    "
    let g:closetag_close_shortcut = '<leader>>'
